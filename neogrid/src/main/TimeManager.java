package main;

import main.entities.Timer;
import java.util.ArrayList;

public class TimeManager {
    ArrayList<Timer> timers = new ArrayList<>();
    ArrayList<ArrayList<Timer>> assemblyTimers = new ArrayList<>();

    public TimeManager(ArrayList<String> Input) {
        generateTimerFromInput(Input);
        generateAssembles();
    }

    public void generateAssembles() {
        int hour;
        int half_hour;
        int forty_five;
        int maintenance;

        for (int i = 0; i < timers.get(0).getCount() + 1; i++) {
            hour = timers.get(0).getTime() * i;
            for (int j = 0; j < timers.get(1).getCount() + 1; j++) {
                half_hour = timers.get(1).getTime() * j;
                for (int k = 0; k < timers.get(2).getCount() + 1; k++) {
                    forty_five = timers.get(2).getTime() * k;
                    for (int m = 0; m < timers.get(3).getCount() + 1; m++) {
                        maintenance = timers.get(3).getTime() * m;

                        int sum = maintenance + forty_five + half_hour + hour;
                        if ((sum < 415) && (sum > 360)) {
                            ArrayList<Timer> PossibleAssembly = new ArrayList<>();
                            PossibleAssembly.add(setTimerValue(i, timers.get(0).getTime()));
                            PossibleAssembly.add(setTimerValue(j, timers.get(1).getTime()));
                            PossibleAssembly.add(setTimerValue(k, timers.get(2).getTime()));
                            PossibleAssembly.add(setTimerValue(m, timers.get(3).getTime()));

                            assemblyTimers.add(PossibleAssembly);
                        }
                    }
                }
            }
        }
    }


    public Timer setTimerValue(int count, int time){
        Timer t = new Timer();
        t.setCount(count);
        t.setTime(time);
        return t;
    }

    public ArrayList<ArrayList<Timer>> getPossibleAssembles(){
        return this.assemblyTimers;
    }


    public void generateTimerFromInput(ArrayList<String> input){
        boolean find;
        for (String Line : input) {
            if (timers.isEmpty()) {
                Timer timer = new Timer();
                if (Line.contains("maintenance")) {
                    timer.setTime(5);
                } else {
                    timer.setTime(Integer.parseInt(Line.replaceAll("[^0-9]", "")));
                }
                timer.setCount(1);
                timers.add(timer);
            } else {
                find = false;
                for (Timer value : timers) {
                    if (Line.contains("maintenance")) {
                        if (5 == value.getTime()) {
                            value.setCount(value.getCount() + 1);
                            find = true;
                            break;
                        }
                    } else {
                        if (Integer.parseInt(Line.replaceAll("[^0-9]", "")) == value.getTime()) {
                            value.setCount(value.getCount() + 1);
                            find = true;
                            break;
                        }
                    }

                }
                if (!find) {
                    Timer timer = new Timer();
                    if (Line.contains("maintenance")) {
                        timer.setTime(5);
                    } else {
                        timer.setTime(Integer.parseInt(Line.replaceAll("[^0-9]", "")));

                    }
                    timer.setCount(1);
                    timers.add(timer);
                }
            }


        }
    }

}