package main;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        FileManager manager = new FileManager();
        manager.OpenFile("neogrid/inputs/input.txt");
        BuildAssembly Assembly = new BuildAssembly();
        manager.GenerateFile("neogrid/outputs/output.txt", Assembly.ReturnMontagem(manager.getFileLines()));
    }
}
