package main;

import java.io.*;
import java.util.ArrayList;

public class FileManager {
    ArrayList<String> FileLines = new ArrayList<String>();

    public void OpenFile(String path) {
        try {
            FileReader arq = new FileReader(path);
            BufferedReader lerArq = new BufferedReader(arq);
            String linha = lerArq.readLine();

            while (linha != null) {
                FileLines.add(linha);
                linha = lerArq.readLine();
            }

            arq.close();
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
    }

    public ArrayList<String> getFileLines(){
        return this.FileLines;
    }


    public void GenerateFile(String name, ArrayList<String> content) throws IOException {
        FileWriter arq = new FileWriter(name);
        PrintWriter gravarArq = new PrintWriter(arq);


        for (String i : content) {
            gravarArq.printf(i + "%n");
        }


        arq.close();
    }
}
