package main;

import main.entities.Timer;

import java.util.ArrayList;

public class BuildAssembly {
    ArrayList<String> Montagens = new ArrayList<String>();
    ArrayList<ArrayList<Timer>> PossibleAssemblys = new ArrayList<ArrayList<Timer>>();

    int totalClock =0;

    public ArrayList<String> ReturnMontagem(ArrayList<String> Input){
        TimeManager time = new TimeManager(Input);
        PossibleAssemblys = time.getPossibleAssembles();

        for (ArrayList<Timer> Assemblys : PossibleAssemblys){
            totalClock =540;
            for (String line : returnInputFilterTolunchTime(Input,totalClock, Assemblys)){
                Montagens.add(line);
            }
            Montagens.add("---------------------------------------");
        }

        return Montagens;


    }

    public ArrayList<String> returnInputFilterTolunchTime(ArrayList<String>Input,int totalClock,ArrayList<Timer> Timers){
        int clock =totalClock;
        String LineOutPut;
        ArrayList<String> OutPut = new ArrayList<>();

        while (clock != 720) {
            for (Timer time : generateAssemblysBeforeLunch(Timers)){
                for (int i = 0; i < time.getCount(); i++) {
                    for (String Line : Input) {
                        if (clock + time.getTime() <= 720) {
                            if (Line.replaceAll("[^0-9]", "") != "") {
                                if (Integer.parseInt(Line.replaceAll("[^0-9]", "")) == time.getTime()) {
                                    if (verifyLineOutputFromInput(OutPut, Line)) {
                                        OutPut.add(returnHourMin(clock)+" "+Line);
                                        clock = clock + time.getTime();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        for(int i = 0; i<Timers.size();i++){
            for (String Line : OutPut) {
                LineOutPut = Line.substring(6,Line.length());
                if (Timers.get(i).getTime() == Integer.parseInt(LineOutPut.replaceAll("[^0-9]", ""))){
                    Timers.get(i).setCount(Timers.get(i).getCount()-1);
                }
            }
        }

        OutPut.add("12:00 Almoço");
        clock = clock + 60;


        for (Timer time : Timers){
            if (time.getCount() > 0 ) {
                for(int i = 0;i< time.getCount();i++){
                    for (String Line : Input) {
                        LineOutPut = Line.substring(6,Line.length());
                        if (LineOutPut.replaceAll("[^0-9]", "") != "") {
                            if (verifyLineOutputFromInput(OutPut, Line)) {
                                if (Integer.parseInt(LineOutPut.replaceAll("[^0-9]", "")) == time.getTime()) {
                                    OutPut.add(returnHourMin(clock)+" "+Line);
                                    clock = clock + time.getTime();
                                    break;
                                }
                            }
                        }
                        else if (Line.contains("maintenance")) {
                            if (verifyLineOutputFromInput(OutPut, Line)) {
                                if (5 == time.getTime()) {
                                    OutPut.add(returnHourMin(clock)+" "+Line);
                                    clock = clock + time.getTime();
                                    break;
                                }
                            }
                        }
                    }
                }
            }

        }

        OutPut.add(returnHourMin(clock) + " Ginástica Laboral");

        return OutPut;
    }

    public Boolean verifyLineOutputFromInput(ArrayList<String> output,String line){
        Boolean find = true;
        String askdjas;
        for(String lineOutput : output){
            askdjas = lineOutput.substring(6,lineOutput.length());
            if (line.equals(askdjas)){
                find = false;
            }
        }
        return find;
    }

    public String returnHourMin(int min) {
        int hours = min / 60;
        int minutes = min % 60;


        if (Integer.toString(minutes).length() == 1) {
            if (Integer.toString(hours).length() == 1) {
                return "0"+Integer.toString(hours) + ":0" + Integer.toString(minutes);
            }
            else{
                return Integer.toString(hours) + ":0" + Integer.toString(minutes);
            }
        }
        else{
            if (Integer.toString(hours).length() == 1) {
                return "0"+Integer.toString(hours) + ":" + Integer.toString(minutes);
            }
            else{
                return Integer.toString(hours) + ":" + Integer.toString(minutes);
            }
        }
    }

    public ArrayList<Timer>  generateAssemblysBeforeLunch( ArrayList<Timer> timers) {
        int hour = 0;
        int half_hour = 0;
        int forty_five = 0;
        int maintenance = 0;
        ArrayList<Timer> PossibleAssembly = new ArrayList<Timer>();

        for (int i = 0; i < timers.get(0).getCount() + 1; i++) {
            hour = timers.get(0).getTime() * i;
            for (int j = 0; j < timers.get(1).getCount() + 1; j++) {
                half_hour = timers.get(1).getTime() * j;
                for (int k = 0; k < timers.get(2).getCount() + 1; k++) {
                    forty_five = timers.get(2).getTime() * k;
                    for (int m = 0; m < timers.get(3).getCount() + 1; m++) {
                        maintenance = timers.get(3).getTime() * m;

                        int sum = maintenance + forty_five + half_hour + hour;
                        if (sum == 180) {
                            if (PossibleAssembly.isEmpty()) {
                                PossibleAssembly.add(setTimerValue(i, timers.get(0).getTime()));
                                PossibleAssembly.add(setTimerValue(j, timers.get(1).getTime()));
                                PossibleAssembly.add(setTimerValue(k, timers.get(2).getTime()));
                                PossibleAssembly.add(setTimerValue(m, timers.get(3).getTime()));
                            }
                        }
                    }
                }
            }
        }

        return PossibleAssembly;
    }
    public Timer setTimerValue(int count, int time){
        Timer t = new Timer();
        t.setCount(count);
        t.setTime(time);
        return t;
    }

}
