package test;
import main.FileManager;
import main.TimeManager;
import main.entities.Timer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;

public class TimerManagerTest {
    FileManager manager = new FileManager();


    @Before
    public void Before() throws IOException {
        manager.OpenFile("inputs/input.txt");
    }

    @Test
    public void testSetTimerValue(){
        TimeManager timeManager = new TimeManager(manager.getFileLines());

        Timer timer = new Timer();
        timer.setTime(60);
        timer.setCount(3);

        Timer timerM = timeManager.setTimerValue(3,60);

        Assert.assertEquals(timer.getCount(),timerM.getCount());
        Assert.assertEquals(timer.getTime(),timerM.getTime());
    }

}
