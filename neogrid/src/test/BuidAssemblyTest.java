package test;

import main.BuildAssembly;
import main.FileManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

public class BuidAssemblyTest {
    BuildAssembly buildAssembly = new BuildAssembly();
    FileManager manager = new FileManager();
    FileManager managerOutPut = new FileManager();


    @Test
    public void testReturnHourMin(){
        int min = 1802;
        String Hours = buildAssembly.returnHourMin(min);
        Assert.assertEquals("30:02",Hours);

    }

    @Test
    public void testVerifyLineOutputFromInput(){
        ArrayList<String> Output = new ArrayList<>();
        String LineOutPut = "Cutting of steel sheets 60min";
        Output.add(LineOutPut);
        String Line = "Tempering sub-zero (Heat treatment) 45min";

        Assert.assertTrue(buildAssembly.verifyLineOutputFromInput(Output,Line));

    }

    @Test
    public void testReturnMontagem(){
        manager.OpenFile("inputs/input.txt");
        managerOutPut.OpenFile("outputs/outputTest.txt");
        ArrayList<String> possibleAssemble = buildAssembly.ReturnMontagem(manager.getFileLines());
        ArrayList<String> possibleAseembleOutPut = managerOutPut.getFileLines();
        Assert.assertArrayEquals(new ArrayList[]{possibleAssemble}, new ArrayList[]{possibleAseembleOutPut});
    }

    @Before
    public void Before(){
        System.out.println("Iniciando teste");
    }

    @After
    public void After(){
        System.out.println("Finalizando teste");
    }
}
